# Instructions

### Registry server -> open console tab
* cd PROJECT_DIR
* mvn install
* cd registry
* mvn exec:java

### Stock service server ->open another console tab
* cd PROJECT_DIR
* cd stockservice
* mvn exec:java [ -Dserver.port=STOCK_PORT] # (default port 7100)

### Nasdaq client server -> open another console tab
* cd PROJECT_DIR
* cd nasdaqclient
* mvn exec:java [ -Dserver.port=STOCK_PORT] # (default port 7200)

Every console will have its own logs. 

It can be multiples instances of stock service of nasdaq client running in different ports. But, as stock service has a static collection, modifications on a instance are not shared with others. This happens because I don't have a centralised db or cache system.

## API endpoint
### GET http://localhost:7200/stock/nasdaq/Bloomberg
This query goes from the nasdaq client (7200) to the StockService (7100) and returns to the nasdaq client

### POST http://localhost:7100/stock/
{     
    "company" : {  
        "name": "Bloomberg"   
    }, 
    "priceValue": 2000,
    "currency": "EUR" 
}
This query goes directly to the StockService (7100), it is not exposed


## Tests
There are only jmeter tests. I have no experience testing microservices properly but it is something that I want to solve