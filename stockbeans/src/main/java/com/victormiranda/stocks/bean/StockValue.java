package com.victormiranda.stocks.bean;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Date;

/**
 * Created by victor on 26/08/15.
 */
public class StockValue {
    private Date date;
    private Double value;
    private Currency currency;

    public StockValue() {

    }

    public StockValue(Date date, double value, Currency currency) {
        this.date = date;
        this.value = value;
        this.currency = currency;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
