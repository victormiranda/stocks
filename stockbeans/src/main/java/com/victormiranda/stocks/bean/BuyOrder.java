package com.victormiranda.stocks.bean;

import java.util.Currency;

/**
 * Created by victor on 27/08/15.
 */
public class BuyOrder {

    Company company;

    Double priceValue;

    Currency currency;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Double getPriceValue() {
        return priceValue;
    }

    public void setPriceValue(Double priceValue) {
        this.priceValue = priceValue;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
