package com.victormiranda.stocks.bean;

/**
 * Created by victor on 26/08/15.
 */
public class CompanyStockValue {
    private Company company;
    private StockValue value;

    public CompanyStockValue() {

    }

    public CompanyStockValue(Company company, StockValue stockValue) {
        this.company = company;
        this.value = stockValue;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public StockValue getValue() {
        return value;
    }

    public void setValue(StockValue value) {
        this.value = value;
    }
}
