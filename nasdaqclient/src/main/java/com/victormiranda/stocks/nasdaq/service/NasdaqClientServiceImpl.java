package com.victormiranda.stocks.nasdaq.service;

import com.victormiranda.stocks.bean.CompanyStockValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by victor on 26/08/15.
 */
@Service
public class NasdaqClientServiceImpl implements NasdaqClientService {

    @Autowired
    protected RestTemplate stockRestTemplate;

    @Override
    public CompanyStockValue getCompanyStockValue(String companyName) {
        return stockRestTemplate.getForObject("http://STOCK-SERVICE/stock/" + companyName, CompanyStockValue.class);
    }
}
