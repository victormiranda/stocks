package com.victormiranda.stocks.nasdaq.controller;

import com.victormiranda.stocks.bean.CompanyStockValue;
import com.victormiranda.stocks.nasdaq.service.NasdaqClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.logging.Logger;

/**
 * This client doesn't expose the "buy order" action, it is a read only client
 * Created by victor on 26/08/15.
 */
@RestController
public class NasdaqClientController {

    private static final Logger LOGGER = Logger.getLogger(NasdaqClientController.class.getName());

    @Autowired
    private NasdaqClientService nasdaqClientService;

    @PostConstruct
    public void postConstructLogging() {
        LOGGER.info("NasdaqClientController constructed");
    }

    @RequestMapping("/stock/nasdaq/{companyName}")
    @ResponseBody
    public CompanyStockValue getCompanyStockValue(@PathVariable("companyName") String companyName) {
        LOGGER.info("get company stock value from company " + companyName);

        return nasdaqClientService.getCompanyStockValue(companyName);
    }

}
