package com.victormiranda.stocks.nasdaq.service;

import com.victormiranda.stocks.bean.CompanyStockValue;

/**
 * Created by victor on 26/08/15.
 */
public interface NasdaqClientService {

    CompanyStockValue getCompanyStockValue(String companyName);
}
