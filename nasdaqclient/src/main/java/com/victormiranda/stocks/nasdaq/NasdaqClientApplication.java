package com.victormiranda.stocks.nasdaq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import java.util.logging.Logger;

/**
 * Created by victor on 26/08/15.
 */
@SpringBootApplication
@EnableDiscoveryClient
public class NasdaqClientApplication {

    private static final Logger LOGGER = Logger.getLogger(NasdaqClientApplication.class.getName());

    public static void main(String[] args) {
        LOGGER.info("Starting NasdaqClient Application");
        SpringApplication.run(NasdaqClientApplication.class, args);
    }
}
