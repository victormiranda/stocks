package com.victormiranda.stocks.registry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

import java.util.logging.Logger;

/**
 * Created by victor on 26/08/15.
 */
@SpringBootApplication
@EnableEurekaServer
public class RegistryApplication {

    private static final Logger LOGGER = Logger.getLogger(RegistryApplication.class.getName());

    public static void main(String[] args) {
        LOGGER.info("Starting Registry application");
        SpringApplication.run(RegistryApplication.class, args);
    }
}
