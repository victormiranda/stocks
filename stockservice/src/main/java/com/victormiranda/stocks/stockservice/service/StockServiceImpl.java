package com.victormiranda.stocks.stockservice.service;

import com.victormiranda.stocks.bean.BuyOrder;
import com.victormiranda.stocks.bean.Company;
import com.victormiranda.stocks.bean.CompanyStockValue;
import com.victormiranda.stocks.bean.StockValue;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.logging.Logger;

/**
 * Created by victor on 26/08/15.
 */
@Service
public class StockServiceImpl implements StockService {

    private static final Logger LOGGER = Logger.getLogger(StockServiceImpl.class.getName());

    //this would be data extracted from other system.
    private static Map<Company, StockValue> stockValues = new HashMap<>();

    static {
        LOGGER.info("Fake data");
        Company bloomberg = new Company(1, "Bloomberg");
        Company google = new Company(2, "Google");

        Date now = new Date();

        stockValues.put(bloomberg, new StockValue(now, 1000, Currency.getInstance("EUR")));
        stockValues.put(google, new StockValue(now, 560d, Currency.getInstance("EUR")));
    }


    @Override
    public CompanyStockValue getCompanyStockValue(String companyName) {
        Optional<Company> company = getCompanyByName(companyName);

        if (company.isPresent()) {
            Company companyFound = company.get();
            //get current stock from that company
            return new CompanyStockValue(companyFound, stockValues.get(companyFound));
        }

        throw new IllegalArgumentException("Company not found: " + companyName);
    }

    @Override
    public CompanyStockValue buyOrder(BuyOrder buyRequest) {
        //TODO NPE check
        Optional<Company> company = getCompanyByName(buyRequest.getCompany().getName());

        if (company.isPresent()) {
            Company companyFound = company.get();

            Date now = new Date();
            StockValue valueToSave = new StockValue(now, buyRequest.getPriceValue(), buyRequest.getCurrency());
            StockValue stockRecoveredAfterUpdate = null;

            //with a lock time of +-50 ms, and 200 threads allowed in tomcat, we expect a max throughput of 20 request/sec
            //the problem is that with a big numbers of request per second, the average time is too high
            synchronized (this) {
                saveStockValue(companyFound, valueToSave);
                //we return the saved value. I do this in two steps because it will be easier to check the race condition
                stockRecoveredAfterUpdate = stockValues.get(companyFound);
            }

            if (isRaceCondition(buyRequest, stockRecoveredAfterUpdate)) {
                LOGGER.warning("Race condition :(");
            }

            return new CompanyStockValue(companyFound, stockRecoveredAfterUpdate);
        }

        throw new IllegalArgumentException("Company not found: " + buyRequest.getCompany());
    }

    private void saveStockValue(Company company, StockValue valueToSave) {
        //simulate some processing like saving to a db

        try {
            Thread.sleep(new Random().nextInt(100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        stockValues.put(company, valueToSave);
    }

    private boolean isRaceCondition(BuyOrder buyOrder, StockValue stockRecovered) {
        return !buyOrder.getPriceValue().equals(stockRecovered.getValue());
    }

    private Optional<Company> getCompanyByName(String name) {
        return stockValues.entrySet().stream()
                .map( e -> e.getKey())
                .filter(k -> k.getName().equals(name))
                .findFirst();
    }
}
