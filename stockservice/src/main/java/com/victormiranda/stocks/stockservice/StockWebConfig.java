package com.victormiranda.stocks.stockservice;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by victor on 26/08/15.
 */
@SpringBootApplication
@ComponentScan("com.victormiranda.stocks.stockservice")
public class StockWebConfig {

}
