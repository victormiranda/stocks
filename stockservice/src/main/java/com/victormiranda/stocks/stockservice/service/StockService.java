package com.victormiranda.stocks.stockservice.service;

import com.victormiranda.stocks.bean.BuyOrder;
import com.victormiranda.stocks.bean.CompanyStockValue;

/**
 * Created by victor on 26/08/15.
 */
public interface StockService {

    CompanyStockValue getCompanyStockValue(String companyName);

    CompanyStockValue buyOrder(BuyOrder buyRequest);
}
