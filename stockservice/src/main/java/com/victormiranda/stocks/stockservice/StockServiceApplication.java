package com.victormiranda.stocks.stockservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Import;

import java.util.logging.Logger;

/**
 * Created by victor on 26/08/15.
 */
@EnableAutoConfiguration
@EnableDiscoveryClient
@Import(StockWebConfig.class)
public class StockServiceApplication {

    private static final Logger LOGGER = Logger.getLogger(StockServiceApplication.class.getName());

    public static void main(String[] args) {
        LOGGER.info("Starting StockService application");
        SpringApplication.run(StockServiceApplication.class, args);
    }

}
