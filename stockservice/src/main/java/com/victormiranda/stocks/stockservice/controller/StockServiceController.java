package com.victormiranda.stocks.stockservice.controller;

import com.victormiranda.stocks.bean.BuyOrder;
import com.victormiranda.stocks.bean.CompanyStockValue;
import com.victormiranda.stocks.stockservice.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.logging.Logger;

/**
 * Created by victor on 26/08/15.
 */
@RestController
public class StockServiceController {

    private static final Logger LOGGER = Logger.getLogger(StockServiceController.class.getName());

    @Autowired
    private StockService stockService;

    @RequestMapping("/stock/{companyName}")
    @ResponseBody
    public CompanyStockValue fetchStockByCompany(@PathVariable("companyName") String companyName) {
        LOGGER.info("Fetching stock from company");

        return stockService.getCompanyStockValue(companyName);
    }

    /**
     *
     * @param buyRequest
     * @return
     */
    @RequestMapping(value = "/stock/", method = RequestMethod.POST)
    @ResponseBody
    public CompanyStockValue setCompanyStock(@RequestBody BuyOrder buyRequest) {
        LOGGER.info("Saving stock of company" + buyRequest.getCompany());

        return stockService.buyOrder(buyRequest);
    }

}
